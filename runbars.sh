#!/bin/sh

XRES_BGC=$(xgetres color0)
XRES_WLC=$(xgetres color11)
XRES_WLC_UL=$(xgetres color3)

export POLYBAR_DOWNUP_LABEL=" %signal:3%% %{F$XRES_BGC}%{B$XRES_WLC} d:%{B$XRES_BGC}%{F-}%downspeed:9% %{F$XRES_BGC}%{B$XRES_WLC_UL} u:%{B$XRES_BGC}%{F-}%upspeed:9% "

polybar-msg cmd quit

polybar base &
